from datetime import datetime

from .xml_service import (
    filter_data_set,
    serialize_xml_data,
    retrieve_data,
)


def retrieve_and_filter_offers(earliest_departure_time, earliest_return_time, min_price, max_price, rating):
    filters = {
        'earliest_departure_time': datetime.strptime(earliest_departure_time, '%H:%M').time(),
        'earliest_return_time': datetime.strptime(earliest_return_time, '%H:%M').time(),
        'min_price': min_price,
        'max_price': max_price,
        'rating': rating,
    }
    data = retrieve_data()
    filtered_data = filter_data_set(data, filters)
    serialized_data = serialize_xml_data(filtered_data)
    return serialized_data
