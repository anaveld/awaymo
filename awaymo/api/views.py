from rest_framework.response import Response
from rest_framework.views import APIView

from .handlers import retrieve_and_filter_offers


class OfferView(APIView):

    def get(self, request):
        earliest_departure_time = request.query_params.get('earliest_departure_time', '00:00')
        earliest_return_time = request.query_params.get('earliest_return_time', '00:00')
        min_price = request.query_params.get('min_price')
        max_price = request.query_params.get('max_price')
        rating = request.query_params.get('star_rating')
        results = retrieve_and_filter_offers(earliest_departure_time, earliest_return_time, min_price, max_price, rating)
        return Response(results)
