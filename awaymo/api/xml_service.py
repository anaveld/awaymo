import requests
from datetime import datetime

from xml.etree import ElementTree

from django.conf import settings
from rest_framework import status


def retrieve_data():
    r = requests.get(settings.XML_DATA_URL)
    if r.status_code == status.HTTP_200_OK:
        tree = ElementTree.fromstring(r.content)
        results = tree.find('Results')
        return results.getchildren()

    return []


def get_summary(data):
    if not data:
        return {}

    price_counter = 0
    max_price = 0
    min_price = 1000000
    for entry in data:
        price = entry.get('Sellprice')
        if not price:
            continue

        price = float(price)
        price_counter += price

        if price > max_price:
            max_price = price

        if price < min_price:
            min_price = price

    return {
        'most_expensive_price': max_price,
        'cheapest_price': min_price,
        'average_price': price_counter/len(data)
    }


def check_entry(entry, filters):
    earliest_departure_time = filters.get('earliest_departure_time')
    departure_time = datetime.strptime(entry.get('Outbounddep'), '%d/%m/%Y %H:%M').time()
    if earliest_departure_time and departure_time < earliest_departure_time:
        return False

    earliest_return_time = filters.get('earliest_return_time')
    return_time = datetime.strptime(entry.get('Inboundarr'), '%d/%m/%Y %H:%M').time()
    if earliest_return_time and return_time < earliest_return_time:
        return False

    rating = filters.get('rating')
    if rating and entry.get('Starrating') < rating:
        return False

    price = entry.get('Sellprice')
    if not price:
        return True

    price = float(price)
    min_price = filters.get('min_price')
    if min_price and price < float(min_price):
        return False

    max_price = filters.get('max_price')
    if max_price and price > float(max_price):
        return False

    return True


def filter_data_set(data, filters):
    results = [entry for entry in data if check_entry(entry, filters)]
    return results


def serialize_xml_data(data):
    offers = [{
        'Sellprice': entry.get('Sellprice'),
        'Starrating': entry.get('Starrating'),
        'Hotelname': entry.get('Hotelname'),
        'Inboundfltnum': entry.get('Inboundfltnum'),
        'Outboundfltnum': entry.get('Outboundfltnum'),
        'Outbounddep': entry.get('Outbounddep'),
        'Inboundarr': entry.get('Inboundarr'),
    } for entry in data]
    return {
        'summary': get_summary(data),
        'offers': offers
    }