Project is quite simple. There are, however, few simplifications.
There should be tests both for full endpoints and unit tests for
single functions. Decimal should be used when dealing with money
in every means. Whole stuff could be dockerized. Current setup
is simple - create venv, runserver and you will be able to test 
my task. API params are like requested. Time format in URL is 
'HH:MM'. Feel free to ask my about my choices and mistakes.